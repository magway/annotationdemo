package com.example.version_annotations.controller;

import com.example.version_annotations.dao.UserRepository;
import com.example.version_annotations.model.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(controllers = {UserController.class})
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserRepository userRepository;

    @Test
    void findAll() throws Exception {
        User user1 = new User()
                .setId(1L)
                .setLogin("login1")
                .setFirstName("fname1")
                .setLastName("lname1")
                .setDescription("decription1");
        User user2 = new User()
                .setId(2L)
                .setLogin("login2")
                .setFirstName("fname2")
                .setLastName("lname2")
                .setDescription("decription2");
        Mockito.doReturn(Arrays.asList(user1,user2)).when(userRepository).findAll();

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/user/")
                .param("version", "4"));
        MvcResult mvcResult = resultActions.andReturn();
        System.out.println(mvcResult.getResponse().getContentAsString());
        resultActions
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].login").value(user1.getLogin()));
    }

    @Test
    void findAll2() throws Exception {
        User user1 = new User()
                .setId(1L)
                .setLogin("login1")
                .setFirstName("fname1")
                .setLastName("lname1")
                .setDescription("decription1");
        User user2 = new User()
                .setId(2L)
                .setLogin("login2")
                .setFirstName("fname2")
                .setLastName("lname2")
                .setDescription("decription2");
        Mockito.doReturn(Arrays.asList(user1,user2)).when(userRepository).findAll();

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/user/json")
                .param("version", "4"));
        MvcResult mvcResult = resultActions.andReturn();
        System.out.println(mvcResult.getResponse().getContentAsString());
        resultActions
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].login").value(user1.getLogin()));
    }
}