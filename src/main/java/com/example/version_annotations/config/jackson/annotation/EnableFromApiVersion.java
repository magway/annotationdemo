package com.example.version_annotations.config.jackson.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@ApiVersionRule(ignore = true)
@CftJacksonAnnotation
public @interface EnableFromApiVersion {
    @AliasFor(annotation = ApiVersionRule.class, attribute = "apiVersionTo")
    long value() default 1L;

}
