package com.example.version_annotations.config.jackson;

import com.example.version_annotations.config.jackson.annotation.ApiVersionRule;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import org.springframework.core.annotation.AnnotatedElementUtils;

import java.lang.reflect.AnnotatedElement;
import java.util.Comparator;
import java.util.Set;
import java.util.stream.Stream;

public class CftAnnotationIntrospector
        extends JacksonAnnotationIntrospector {

    private final Long version;

    public CftAnnotationIntrospector(Long version) {
        this.version = version;
    }

    @Override
    public boolean hasIgnoreMarker(AnnotatedMember m) {
        ApiVersionRule apiVersionRule = findVersionRule(m.getAnnotated());
        return apiVersionRule != null ? apiVersionRule.ignore() : super.hasIgnoreMarker(m);
    }

    @Override
    public PropertyName findNameForSerialization(Annotated a) {
        PropertyName propertyName = findFieldName(a);
        return propertyName != null ? propertyName : super.findNameForSerialization(a);
    }

    @Override
    public PropertyName findNameForDeserialization(Annotated a) {
        PropertyName propertyName = findFieldName(a);
        return propertyName != null ? propertyName : super.findNameForDeserialization(a);
    }

    private ApiVersionRule findVersionRule(AnnotatedElement annotatedElement) {
        Set<ApiVersionRule> allMergedAnnotations =
                AnnotatedElementUtils.findAllMergedAnnotations(annotatedElement, ApiVersionRule.class);
        allMergedAnnotations.addAll(
                AnnotatedElementUtils.findMergedRepeatableAnnotations(annotatedElement, ApiVersionRule.class));
        Stream<ApiVersionRule> filteredRules = allMergedAnnotations.stream()
                .filter(rule -> (rule.apiVersionFrom() == -1L || rule.apiVersionFrom() <= version)
                        && (rule.apiVersionTo() == -1L || rule.apiVersionTo() > version));
        return filteredRules
                .max(Comparator.comparingLong(ApiVersionRule::apiVersionFrom))
                .orElse(null);
    }

    private PropertyName findFieldName(Annotated annotated) {
        ApiVersionRule apiVersionRule = findVersionRule(annotated.getAnnotated());
        return apiVersionRule != null ? new PropertyName(apiVersionRule.fieldName()) : null;
    }
}
