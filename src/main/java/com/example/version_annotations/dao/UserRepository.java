package com.example.version_annotations.dao;

import com.example.version_annotations.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository
    extends CrudRepository<User, Long> {

    User findByLogin(final String login);
}
