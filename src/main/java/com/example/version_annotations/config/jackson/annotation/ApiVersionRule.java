package com.example.version_annotations.config.jackson.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Documented
@CftJacksonAnnotation
@Repeatable(ApiVersionRules.class)
@Inherited
public @interface ApiVersionRule {
    long apiVersionFrom() default -1L;

    long apiVersionTo() default -1L;

    boolean ignore() default false;

    String fieldName() default "";
}

