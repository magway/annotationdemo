package com.example.version_annotations.controller;

import com.example.version_annotations.config.jackson.CftAnnotationIntrospector;
import com.example.version_annotations.controller.dto.UserDto;
import com.example.version_annotations.dao.UserRepository;
import com.example.version_annotations.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping(value = "", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "get list of users", description = "retrieve all users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "everething is ok"),
            @ApiResponse(responseCode = "400", description = "some troubles")
    })
    public String findAll(
            @RequestParam("version") Long version
    ) throws JsonProcessingException {
        Iterable<User> all = userRepository.findAll();
        List<UserDto> collect = StreamSupport.stream(all.spliterator(), false)
                .map(this::toDto)
                .toList();
        ObjectMapper objectMapper = new ObjectMapper()
                .setAnnotationIntrospector(new CftAnnotationIntrospector(version));
        return objectMapper.writeValueAsString(collect);
    }

    @GetMapping(value = "/json", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<UserDto> findAllForLastVersion() {
        Iterable<User> all = userRepository.findAll();
        return StreamSupport.stream(all.spliterator(), false)
                .map(this::toDto)
                .toList();
    }

    private UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId())
                .setLogin(user.getLogin())
                .setFirstName(user.getFirstName())
                .setLastName(user.getLastName())
                .setDescription(user.getDescription());
        return userDto;
    }
}
