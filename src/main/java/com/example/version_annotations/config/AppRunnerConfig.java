package com.example.version_annotations.config;

import com.example.version_annotations.dao.UserRepository;
import com.example.version_annotations.model.User;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class AppRunnerConfig {


    @Bean
    public ApplicationRunner applicationRunner(
            UserRepository userRepository) {
        return args -> {
            User user1 = new User()
                    .setId(1L)
                    .setLogin("login1")
                    .setFirstName("fname1")
                    .setLastName("lname1")
                    .setDescription("decription1");
            User user2 = new User()
                    .setId(2L)
                    .setLogin("login2")
                    .setFirstName("fname2")
                    .setLastName("lname2")
                    .setDescription("decription2");
            userRepository.saveAll(Arrays.asList(user1, user2));
        };
    }
}
