package com.example.version_annotations.config.jackson;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JacksonCustomizer {

    @Value("${api.version:1L}")
    private Long apiVersion;
/*

    @Bean
    @Primary
    public ObjectMapper objectMapper() {
        return new ObjectMapper()
                .setAnnotationIntrospector(new CftAnnotationIntrospector(apiVersion));
    }
*/

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
        return jacksonObjectMapperBuilder ->
                jacksonObjectMapperBuilder.annotationIntrospector(new CftAnnotationIntrospector(apiVersion));
    }
}
