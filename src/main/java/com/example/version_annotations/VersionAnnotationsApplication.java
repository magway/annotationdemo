package com.example.version_annotations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VersionAnnotationsApplication {

    public static void main(String[] args) {
        SpringApplication.run(VersionAnnotationsApplication.class, args);
    }

}
