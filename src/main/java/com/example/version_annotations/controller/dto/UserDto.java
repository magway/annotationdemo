package com.example.version_annotations.controller.dto;

import com.example.version_annotations.config.jackson.annotation.ApiVersionRule;
import com.example.version_annotations.config.jackson.annotation.DisableFromApiVersion;
import com.example.version_annotations.config.jackson.annotation.EnableFromApiVersion;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@Schema(name = "User", description = "User data")
public class UserDto
        implements Serializable {

    @EnableFromApiVersion(3)
    private Long id;

    @ApiVersionRule(apiVersionFrom = 2, apiVersionTo = 7, fieldName = "login1")
    @ApiVersionRule(apiVersionFrom = 1, apiVersionTo = 5, fieldName = "login2")
    @Schema(name = "login", description = "User login name")
    private String login;

    private String firstName;

    @ApiVersionRule(apiVersionTo = 5, fieldName = "lname")
    private String lastName;

    @DisableFromApiVersion(3)
    private String description;
}
